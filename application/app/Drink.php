<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drink extends Model
{
    protected $table = 'drinks';

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

}
