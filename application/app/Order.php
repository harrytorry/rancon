<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'orders';

    protected $casts = [
        'drinks' => 'array',
        'completed' => 'boolean'
    ];

    protected $guarded = [];

    public function complete()
    {
        $this->completed = true;
        return $this;
    }
}
