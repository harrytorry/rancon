<?php

namespace App\Http\Controllers;

use App\Drink;
use Illuminate\Http\Request;

class DrinksController extends Controller
{

    /**
     * Show the profile for the given user.
     *
     * @return Response
     */
    public function __invoke()
    {

        $drinks = Drink::all();

        return response()->json(['drinks' => $drinks]);
    }

}
