<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderCompleteController extends Controller
{

    /**
     * Show the profile for the given user.
     *
     * @return Response
     */
    public function __invoke(Order $order)
    {
        $order
            ->complete()
            ->save();

        return response()->json(['order' => $order]);
    }
}
