<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(\App\Order::class, function (Faker\Generator $faker) {

    $numberOfDrinks = $faker->numberBetween(1,6);

    return [
        'user_id' => $faker->numberBetween(1, 10),
        'drinks' => json_encode($faker->randomElements([1, 1, 1, 2, 2, 2, 3, 3, 3], $numberOfDrinks)),
        'completed' => $faker->boolean(25),
    ];
});