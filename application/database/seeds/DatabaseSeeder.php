<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(DrinksSeeder::class);
         $this->command->info('Drinks are seeded.');

         $this->call(OrderSeeder::class);
         $this->command->info('Orders are seeded.');
    }
}
