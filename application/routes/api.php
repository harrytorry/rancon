<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$router
    ->get('drinks', 'DrinksController')
    ->name('drinks.index');

$router->resource('orders', 'OrderController',  ['only' => [
    'index', 'show', 'store', 'edit', 'update'
]]);

$router
    ->post('orders/{order}/complete', 'OrderCompleteController')
    ->name('orders.complete');
