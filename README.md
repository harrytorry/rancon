# Code test
Harry Torry

## Setup (docker)

* run `docker-compose up -d` to initialise all of the projects
* run `docker-compose run --rm composer install` to install dependencies
* run `docker-compose run --rm artisan migrate` to install dependencies
* run `docker-compose run --rm artisan db:seed` to seed the example data

To verify that the app is running, navigate to `http://rancon.jobtest.dev:85/`

You can find the correct port by running `docker ps` in the terminal.

![docker ps](http://i.imgur.com/ArDkFRK.png)

## Structure

To view the routes, run a `docker-compose run --rm artisan route:list`

![docker-compose run --rm artisan route:list](http://i.imgur.com/ASFCr8U.png)

* The router is stored in `application/routes/api.php`
* Controllers are located in `application/app/Http/Controllers`
* Validation is performed on the controller methods by injecting a validator, which is stored in `application/app/Http/Requests`
* The models (`Order` and `Drink`) are stored in `application/app`
